from django.urls import path
from . import views

urlpatterns = [
    path('', views.ShowHome, name="home"),
    path('about', views.ShowAbout, name="about")
]