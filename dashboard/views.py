from django.shortcuts import render
from .models import Dashboard
from django.contrib.auth.decorators import login_required

@login_required
def dashboard(request):
    userid = request.user.id
    inquiries = Dashboard.objects.all().filter(userid=userid)
    context = {
        "inquiries": inquiries
    }
    return render(request, "dashboard/dashboard.html", context)