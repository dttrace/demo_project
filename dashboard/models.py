from django.db import models
from datetime import datetime

class Dashboard(models.Model):
    userid = models.IntegerField()
    productid = models.IntegerField()
    name = models.CharField(max_length = 100)
    phone = models.IntegerField()
    email = models.EmailField(max_length=30)
    message = models.TextField()
  #  inquiry_date = models.DateTimeField()

    def __str__(self):
        return self.name
