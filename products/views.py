from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic.list import ListView
from .models import Product
from dashboard.models import Dashboard


class ProductList(ListView):
    model = Product
    context_object_name = "products"

def search(request):
    if 'features' in request.GET or 'price' in request.GET:
        queryset_list = Product.objects.order_by("-list_date")
    else:
        queryset_list = None
    
    if 'features' in request.GET:
        features = request.GET["features"]
        if features:
            queryset_list = queryset_list.filter(features__icontains=features)
    
    if 'price' in request.GET:
        price = request.GET["price"]
        if price:
            queryset_list = queryset_list.filter(price__lte=price)

    context = {
        'products':queryset_list
    }
    return render(request, "products/search_page.html", context)

def showproduct(request, pk):
    if request.POST:
        if request.user.is_authenticated:
            userid = request.user.id
        else:
            userid = 0
        
        productid = request.POST["productid"]
        productname = request.POST["productname"]
        phone = request.POST["phone"]        
        email = request.POST["email"]
        message = request.POST["message"]

        dash = Dashboard(userid=userid, productid=productid, productname=productname, phone=phone, email=email, message=message)
        dash.save()
        return redirect('dashboard')

    else:
        product = get_object_or_404(Product, pk=pk)

        context = {
            'product':product,
            'type': 'I am using function to show views'
        }
        return render(request, 'products/detail_view.html', context)