from django.db import models
from datetime import datetime
#from .choices import CHARGER_CHOICES, OPERATING_SYSTEM


CHARGER_CHOICES = (
('USB C', 'USB C'),
('Lightning Port', 'Lightning Port'),
('Micro USB', 'Micro USB'),
)

OPERATING_SYSTEM = (
('iOS', 'iOS'),
('Android', 'Android'),
('Blackberry', 'Blackberry')
)
class Product(models.Model):
    
    name = models.CharField(max_length=100)
    description = models.TextField()
    ram = models.IntegerField(default=0)
    storage = models.IntegerField(default=0)
    release = models.DateField(auto_now=datetime.now)
    fiveg = models.BooleanField("5G Capability", default=False)
    stylus = models.BooleanField(default=False)
    screensize = models.TextField("Screen Size")
    cpu = models.TextField(default=' ')
    refresh = models.IntegerField(default=0)
    weight = models.DecimalField(default=0, decimal_places=2, max_digits=4)
    count = models.IntegerField()
    operatingsystem = models.CharField(max_length=20, choices=OPERATING_SYSTEM, blank=True)
    charger = models.CharField(max_length=20, choices=CHARGER_CHOICES, blank=True)
    photo = models.ImageField(upload_to="photos/%Y/%m/&d", blank=True)
    
    def __str__(self):
        return self.name
