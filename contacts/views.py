from django.shortcuts import render, redirect
from .models import Contact


def contactus(request):

    if request.POST:
        if 'name' in request.POST:
            name = request.POST['name']
        if 'email' in request.POST:
            email = request.POST['email']
        if 'phone' in request.POST:
            phone = request.POST['phone']
        if 'message' in request.POST:
            message = request.POST['message']

        mycontact = Contact(name=name, email=email, phone=phone, message=message)
        mycontact.save()
        
        return redirect('home')
    else:
        return render(request, "contacts/contactus.html")