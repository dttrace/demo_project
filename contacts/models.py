from django.db import models

class Contact(models.Model):
    userid = models.IntegerField(blank=True, null=True)
    name = models.CharField(max_length=100)
    email = models.CharField(max_length=50)
    phone = models.CharField(max_length=20)
    message = models.TextField()
    is_contact = models.BooleanField(default=False)
    feedback = models.TextField()

def __str__(self):
  return self.name